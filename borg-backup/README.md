# Borg Backup Reporter

This reporter is designed to monitor the performance of borg backups (https://borgbackup.readthedocs.io/en/stable/index.html)
Specifically, whether they are happening with the correct frequency. Multiple repositories may be configured and will be
checked as needed, as well as appropriate thresholds for how long ago the most recent successful backup occurred before
an alert should be fired.

For example:

```yaml
exchange: skynet
route: mr-bones.system.borg_backups
ttl: 1200
repositories:
    ed-209:/local/borg/mr-bones:
        passphrase: hackme
        local_user: eugene
        warn_threshold_seconds: 172800
        err_threshold_seconds: 432000
        crit_threshold_seconds: 604800
    ed-209:/local/borg/mr-bones:
        passphrase: hackme
        local_user: eugene
        warn_threshold_seconds: 172800
        err_threshold_seconds: 432000
        crit_threshold_seconds: 604800
```