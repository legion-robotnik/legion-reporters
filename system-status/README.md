# System Status Reporter

This reporter is designed to publish information on basic system stats, such as CPU/RAM consumption and disk space usage. This reporter is also capable of alerting on low disk space when properly configured with thresholds. The reporter is designed to run once, and can be configured to run regularly using a service manager like supervisord or systemd.

## Configuration:

The reporter takes in a YAML-formatted file on stdin, for example:

```yaml
"paths": ["/", "/local"],
"queue": "skynet",
"route": "my_server.system.system_base_stats"
```

This example demonstrates a basic configuration which will report CPU/RAM/Disk information to the "skynet" exchange under the routing key approriate for the server. Note that disk information is determined by a list of paths at which the disks are mounted.


If we had the above saved to a file called `config.yaml` then the script could be executed with:

```bash
python3 system-status.py < config.yaml
```

Alternatively:

```yaml
"paths": {"/": {'warning_threshold_used_percent': 90,
                'error_threshold_used_percent': 95,
                'critical_threshold_used_percent': 98}},
"restart_required": {"severity": "warning", "path": "/var/run/reboot-required"}
"queue": "skynet",
"route": "mr-bones.system.system_base_stats"}
```

In this example, we use "paths" as a dictionary with thresholds, for warning, error, and critical.
Each threshold is optional and can be set to any percentage you want. If the disk utilization
becomes equal to or greater than the given threshold, an alert of appropriate priority will be
broadcast. You can also see the "restart_required" setting which will broadcast messages of the given
severity if the /var/run/reboot-required file is detected on the system.