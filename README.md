# Legion Reporters

This is a set of common reporters that are designed to work with the Legion alerting/monitoring system. There is one directory for each reporter, which in turn contains a script and a requirements.txt file.

Please keep in mind that all reporters implicitly expect the following three dependencies in their environment:
- [`robotnikmq`](https://pypi.org/project/robotnikmq/)
- [`legion-utils`](https://pypi.org/project/legion-utils/)
- [`pyyaml`](https://pypi.org/project/PyYAML/)