# System Status Reporter

This reporter is designed to publish information on the status and health of ZFS Pools. It will alert on any pool that is in an undesirable state as defined by the severity configuration section (see below)

## Configuration:

The reporter takes in a YAML-formatted file on stdin, for example:

```yaml
exchange: skynet
route: brainiac.system.zfs
ttl: 1200
severity:
  ONLINE: INFO
  DEGRADED: ERROR
  FAULTED: CRITICAL
  OFFLINE: WARNING
  UNAVAILABLE: CRITICAL
  REMOVED: WARNING
```

Note that the contents of the `severity` section have to account for all possible states as defined in the [ZFS documentation](https://docs.oracle.com/cd/E19253-01/819-5461/gamno/index.html). If an unknown state is detected, then a critical alert will be fired.