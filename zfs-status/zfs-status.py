#!/usr/bin/env python3
from itertools import zip_longest
from pprint import pformat
from re import MULTILINE, compile as regex, sub
from socket import gethostname
from subprocess import check_output
from sys import stdin, stdout
from typing import Dict, Iterable, Union, Optional

from cachetools import cached, TTLCache, LRUCache
from legion_utils import priority_of, Priority, log, broadcast
from typeguard import typechecked
from yaml import safe_load as load_yaml

log.remove()
log.add(stdout, level="DEBUG")
log.enable("robotnikmq")
log.enable("legion_utils")

ZPL_LST_COLS = ('size', 'allocated', 'free', 'capacity', 'health')
ZPL_STS_COLS = ('name', 'state', 'read_errors', 'write_errors', 'checksum_errors', 'notes')
ZPL_STATUS_PAT = regex(r'^\s*(?P<lbl>\S+)\:\s+(?P<val>(([\s\S]+?(?=(\n\s*\S+\:)))|(.+$)))', flags=MULTILINE)

# Parsing zpool list
@cached(cache=TTLCache(maxsize=1, ttl=2))
@typechecked
def _zpool_list() -> str:
    return check_output(['zpool', 'list', '-Ho', 'name,size,alloc,free,cap,health']).decode().strip()

@cached(cache=TTLCache(maxsize=1, ttl=2))
@typechecked
def _zpool_list_split() -> Iterable[Iterable[str]]:
    return (line.split('\t') for line in _zpool_list().split('\n'))

@typechecked
def zpool_list() -> Dict[str, Dict[str, str]]:
    return {l[0]: {k: v for k, v in zip(ZPL_LST_COLS, l[1:])} for l in _zpool_list_split()}

# Parsing zpool status
@cached(cache=TTLCache(maxsize=1, ttl=2))
@typechecked
def _status_output(zpool: str) -> str:
    return check_output(['zpool', 'status', zpool]).decode().strip()

@typechecked
def clean(value: str) -> str:
    return sub(r"\s+", ' ', value).strip()

@typechecked
def maybe_int(val: Optional[str]) -> Union[int, str, None]:
    try:
        return int(val)
    except (ValueError, TypeError):
        return val

@cached(cache=LRUCache(maxsize=32))
@typechecked
def _line(line: str) -> Dict[str, Union[int, str, None]]:
    return {k.strip(): maybe_int(v) for k,v in zip_longest(ZPL_STS_COLS,
                                                           line.strip().split(
                                                               maxsplit=len(ZPL_STS_COLS)-1))}

@typechecked
def _config(config: str) -> Dict[str, Dict[str, Union[str, int, None]]]:
    return {_line(line)['name']: _line(line) for line in config.split('\n')[1:]}

@typechecked
def zpool_status(zpool: str) -> Dict[str, Union[str, Dict[str, Dict[str, Union[str, int, None]]]]]:
    return {m['lbl']: (_config(m['val']) if m['lbl'] == 'config' else clean(m['val']))
            for m in ZPL_STATUS_PAT.finditer(_status_output(zpool))}

# Gathering Information & Publishing
@typechecked
def health_priority(health: str, config: Dict[str, str]) -> Priority:
    return priority_of(config.get(health)) if health in config else Priority.CRITICAL

@typechecked
def zpool_info() -> Dict[str, Union[str, Dict[str, Union[str, Dict[str, Dict[str, Union[str, int, None]]]]]]]:
    result = zpool_list()
    for name, zpool in result.items():
        zpool['status'] = zpool_status(name)
    return result

def main(config):
    HOSTNAME = gethostname().lower()
    zfs_info = zpool_info()
    # broadcast basic information
    broadcast(exchange=config['exchange'],
              priority=Priority.INFO,
              route=config['route'],
              contents={'hostname': HOSTNAME,
                        'zfs': zfs_info},
              ttl=config['ttl'])
    # broadcast about degradations on a per-pool basis
    for zpool in zfs_info.keys():
        health = zfs_info[zpool]["health"]
        priority = health_priority(health, config['severity'])
        broadcast(exchange=config['exchange'],
                  priority=priority,
                  route=config['route'],
                  description=f'ZPool [{zpool}] on {HOSTNAME} is {health}',
                  alert_key=f'[{HOSTNAME}][zfs][{zpool}][undesirable_state]',
                  contents={'hostname': gethostname().lower(),
                            'zfs': zfs_info},
                  ttl=config['ttl'])

    
if __name__ == '__main__':
    main(load_yaml(stdin.read()))
