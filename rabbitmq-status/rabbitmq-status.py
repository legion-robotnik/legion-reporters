from json import loads as load_json
from functools import partial
from re import compile as regex, S, M
import subprocess
from sys import stdin
from typing import Optional, List, Tuple, Dict

import legion_utils
from legion_utils import Priority, priority_of, AlertMsg, log
from yaml import safe_load as load_yaml


log.enable("robotnikmq")
log.enable("legion_utils")


class RabbitMQClusterStatus:
    @property
    def cluster_name(self) -> str:
        return self.cluster_status['cluster_name']

    @property
    def disk_nodes(self) -> List[str]:
        return self.cluster_status['disk_nodes']

    @property
    def running_nodes(self) -> List[str]:
        return self.cluster_status['running_nodes']

    @property
    def network_partitions(self) -> List[str]:
        return self.cluster_status['partitions']

    @property
    def alarms(self) -> List[str]:
        return self.cluster_status['alarms']

    @property
    def versions(self) -> Dict[str, Dict[str, str]]:
        return self.cluster_status['versions']

    def __init__(self, output: str):
        self.cluster_status = load_json(output)

    def alert_for_disk_nodes(self, expected_nodes: List[str], ttl: int,
                             priority: Optional[Priority] = None) -> Optional[AlertMsg]:
        priority = priority or Priority.WARNING
        if priority >= Priority.WARNING and not set(self.disk_nodes) >= set(expected_nodes):
            return AlertMsg(contents={'expected_disk_nodes': expected_nodes,
                                      'actual_disk_nodes': list(self.disk_nodes)},
                            key=f'[{self.cluster_name}][rabbitmq][disk_node_mismatch]',
                            desc=f'Not all expected disk nodes are available in {self.cluster_name} cluster',
                            ttl=ttl, priority=priority)
        return None

    def alert_for_running_nodes(self, expected_nodes: List[str], ttl: int,
                                priority: Optional[Priority] = None) -> Optional[AlertMsg]:
        priority = priority or Priority.WARNING
        if priority >= Priority.WARNING and not set(self.running_nodes) >= set(expected_nodes):
            return AlertMsg(contents={'expected_running_nodes': expected_nodes,
                                      'actual_running_nodes': list(self.running_nodes)},
                            key=f'[{self.cluster_name}][rabbitmq][running_node_mismatch]',
                            desc=f'Not all expected running nodes are available in {self.cluster_name} cluster',
                            ttl=ttl, priority=priority)
        return None

    def alert_for_alarms(self, ttl: int, priority: Optional[Priority] = None) -> Optional[AlertMsg]:
        priority = priority or Priority.WARNING
        if priority >= Priority.WARNING and self.alarms:
            return AlertMsg(contents={'alarms': self.alarms},
                            key=f'[{self.cluster_name}][rabbitmq][alarms]',
                            desc=f'RabbitMQ Alarms in {self.cluster_name} cluster',
                            ttl=ttl, priority=priority)
        return None

    def alert_for_network_partition(self, ttl: int,
                                    priority: Optional[Priority] = None) -> Optional[AlertMsg]:
        priority = priority or Priority.WARNING
        if priority >= Priority.WARNING and self.network_partitions:
            return AlertMsg(contents={'network_partitions': self.network_partitions},
                            key=f'[{self.cluster_name}][rabbitmq][network_partitions]',
                            desc=f'RabbitMQ network partition in {self.cluster_name} cluster',
                            ttl=ttl, priority=priority)
        return None

    def alert_for_version_mismatch(self, ttl: int,
                                   priority: Optional[Priority] = None) -> Optional[AlertMsg]:
        priority = priority or Priority.WARNING
        if len({v['erlang_version'] for v in self.versions.values()}) > 1 or len({v['rabbitmq_version'] for v in self.versions.values()}) > 1:
            return AlertMsg(contents={'versions': self.versions},
                            key=f'[{self.cluster_name}][rabbitmq][version_mismatch]',
                            desc=f'RabbitMQ versions in {self.cluster_name} do not match',
                            ttl=ttl, priority=priority)
        return None


def cluster_status() -> RabbitMQClusterStatus:
    return RabbitMQClusterStatus(subprocess.check_output(['rabbitmqctl', 'cluster_status', '--formatter', 'json'])
                                 .decode().replace('\x1b[1m', '').replace('\x1b[0m', ''))


def disk_node_expectations(config) -> Optional[Tuple[List[str], int, Priority]]:
    if 'expected_disk_nodes' in config and 'missing_disk_node' in config['alert_priorities']:
        return (config['expected_disk_nodes'], config['ttl'],
                priority_of(config['alert_priorities']['missing_disk_node']))
    return None


def running_node_expectations(config) -> Optional[Tuple[List[str], int, Priority]]:
    if 'expected_running_nodes' in config and 'missing_running_node' in config['alert_priorities']:
        return (config['expected_running_nodes'], config['ttl'],
                priority_of(config['alert_priorities']['missing_running_node']))
    return None


def version_mismatch_priority(config) -> Optional[Priority]:
    if 'alert_priorities' in config and 'version_mismatch' in config['alert_priorities']:
        return priority_of(config['alert_priorities']['version_mismatch'])
    return None


def alarm_priority(config) -> Optional[Priority]:
    if 'alert_priorities' in config and 'alarm' in config['alert_priorities']:
        return priority_of(config['alert_priorities']['alarm'])
    return None


def network_partition_priority(config) -> Optional[Priority]:
    if 'alert_priorities' in config and 'network_partition' in config['alert_priorities']:
        return priority_of(config['alert_priorities']['network_partition'])
    return None


def main(config) -> None:
    status = cluster_status()
    broadcast = partial(legion_utils.broadcast_alert_msg, config['exchange'], config['route'])
    if (exp := disk_node_expectations(config)) and (alert := status.alert_for_disk_nodes(*exp)):
        broadcast(alert)
    if (exp := running_node_expectations(config)) and \
       (alert := status.alert_for_running_nodes(*exp)):
        broadcast(alert)
    if (priority := version_mismatch_priority(config)) and \
       (alert := status.alert_for_version_mismatch(config['ttl'], priority)):
        broadcast(alert)
    if (priority := alarm_priority(config)) and \
       (alert := status.alert_for_alarms(config['ttl'], priority)):
        broadcast(alert)
    if (priority := network_partition_priority(config)) and \
       (alert := status.alert_for_network_partition(config['ttl'], priority)):
        broadcast(alert)


if __name__ == '__main__':
    main(load_yaml(stdin.read()))