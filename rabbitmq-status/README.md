# RabbitMQ Status Reporter

This reporter publishes information about the status of RabbitMQ running on the machine. Note that `rabbitmqctl` needs to be installed and configured in order for this to work properly.

## Configuration:

The reporter takes in a YAML-formatted file on stdin, for example:

```yaml
exchange: skynet,
route: mr-bones.service.rabbitmq,
ttl: 60,
expected_disk_nodes: [rabbit@burn-e, rabbit@legion, rabbit@m-o, rabbit@matrix],
expected_running_nodes: [rabbit@burn-e, rabbit@legion, rabbit@m-o, rabbit@matrix],
alert_priorities:
    missing_disk_node: error,
    missing_running_node: error,
    version_mismatch: warning,
    alarm: critical,
    network_partition: error
```

This example demonstrates a basic configuration which will report the number of upgradable packages and alert on them if the number of security packages is above 3 with a warning or an error if its above 10.


If we had the above saved to a file called `config.yaml` then the script could be executed with:

```bash
python3 rabbitmq-status.py < config.yaml
```
