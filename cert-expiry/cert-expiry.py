from datetime import datetime
from pathlib import Path
from socket import gethostname
from sys import stdin
from typing import Dict, List, Union

import arrow
from arrow.arrow import Arrow
from cryptography import x509
from legion_utils import broadcast_info, broadcast_warning, broadcast_error, broadcast_critical, log
from yaml import safe_load as load_yaml


log.enable("robotnikmq")
log.enable("legion_utils")


def _not_valid_after(cert_path: Path) -> Arrow:
    cert = x509.load_pem_x509_certificate(cert_path.open('rb').read())
    return arrow.get(cert.not_valid_after)


def main(config):
    for cert_file in config['paths']:
        cert_path = Path(cert_file).resolve()
        if not cert_path.exists():
            broadcast_error(exchange=config['exchange'],
                            route=config['route'],
                            desc=f'Certificate at {gethostname()}:{cert_path} does not exist',
                            alert_key=f'[{gethostname()}][{cert_path}][does_not_exist]',
                            contents={'src': gethostname(),
                                      'cert_path': str(cert_path)},
                            ttl=config['ttl'])
        else:
            not_valid_after = _not_valid_after(cert_path)
            expiry = (not_valid_after - arrow.now()).days
            report = {'src': gethostname(),
                      'cert_path': str(cert_path),
                      'not_valid_after': not_valid_after.int_timestamp}
            desc = f'Certificate at {gethostname()}:{cert_path} expires in {expiry} days' if \
                   expiry >= 0 else f'Certificate at {gethostname()}:{cert_path} has expired!'
            if config['expiry_critical_threshold'] >= expiry:
                broadcast_critical(exchange=config['exchange'],
                                   route=config['route'],
                                   desc=desc,
                                   alert_key=f'[{gethostname()}][{cert_path}][expiry]',
                                   contents=report,
                                   ttl=config['ttl'])
            elif config['expiry_error_threshold'] >= expiry:
                broadcast_error(exchange=config['exchange'],
                                route=config['route'],
                                desc=desc,
                                alert_key=f'[{gethostname()}][{cert_path}][expiry]',
                                contents=report,
                                ttl=config['ttl'])
            elif config['expiry_warning_threshold'] >= expiry:
                broadcast_warning(exchange=config['exchange'],
                                  route=config['route'],
                                  desc=desc,
                                  alert_key=f'[{gethostname()}][{cert_path}][expiry]',
                                  contents=report,
                                  ttl=config['ttl'])
            else:
                broadcast_info(exchange=config['exchange'], route=config['route'], contents=report)


if __name__ == '__main__':
    main(load_yaml(stdin.read()))
