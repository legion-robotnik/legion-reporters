# Cert Expiry Reporter

This reporter takes a configuration object that is effectively a list of paths at which certificates
are to be found as well as the thresholds for expiry dates which are used to determine whether to
fire a warning, error, or critical. It also, obviously, takes a TTL, exchange, and route to use.

If a certificate expires in fewer days than a given threshold, then this reporter will fire an alert
of appropriate priority. Otherwise, it simply broadcasts info.

For example:

```yaml
exchange: skynet
route: my_server.network.cert_expiry
ttl: 600,
paths: [/etc/tls/https.cert, /var/www/my_app/my_cert.pem]
expiry_warning_threshold: 30
expiry_error_threshold: 10
expiry_critical_threshold: 1
```

In this example, the reporter will check two different certificates, and if any of them expire in
less than or equal to 30 days, it will fire a warning, an error for 10 days, and a critical for one
day. Otherwise it will simply broadcast the information. It will also broadcast critical if a given
certificate file does not exist. Each alert will last 10 minutes (600 seconds) unless cleared.

If we had the above saved to a file called `config.yaml` then the script could be executed with:

```bash
python3 apt-check.py < config.yaml
```