# Unpingable Reporter

This feedback reporter gathers statistics on which hosts are unpingable based on information from the pingable reporter. It takes a configuration which tells it how many sources must be unable to ping a given target before a warning, error, or critical alert is fired. Any number of sources that cannot ping a given target will produce at least an info broadcast.

Example Configuration:

```yaml
observe_exchange: skynet
observe_binding: #.network.pingable.info
report_exchange: skynet
report_route_suffix: .network.unpingable
ttl: 60
timeout: 3600
unpingable_from_critical: 4
unpingable_from_error: 3
unpingable_from_warning: 2
```

The example configuration above would monitor a given exchange/binding and when 4 machines cannot ping the same target, this would set off a critical alert, 3 for an erro, and 2 for a warning. Hosts that are targeted for pinging are gathered dynamically from the ping reports, however, if a machine is not reported on (e.g. neither pingable or unpingable) for 3,600 seconds (an hour) it will no longer cause unpingable alerts to be emitted.

If we had the above saved to a file called `config.yaml` then the script could be executed with:

```bash
python3 unpingable.py < config.yaml
```