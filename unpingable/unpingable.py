#!/usr/bin/env python3
from threading import Thread, Lock
from time import sleep
from typing import Dict
from sys import stdin

from arrow import now, Arrow
from legion_utils import broadcast_critical, broadcast_error, broadcast_warning, broadcast_info, log
from robotnikmq import Subscriber, Message
from yaml import safe_load as load_yaml

log.enable("robotnikmq")
log.enable("legion_utils")


def main(config: Dict) -> None:
    obsrv_ex = config['observe_exchange']
    obsrv_bind = config['observe_binding']
    rprt_ex = config['report_exchange']
    rprt_route_suffix = config['report_route_suffix']

    ping_targets: Dict[str, Dict[str, bool]] = {}
    last_reported: Dict[str, Arrow] = {}
    network_state_lock = Lock()

    def sub(msg: Message) -> None:
        with network_state_lock:
            if 'pingable' in msg and 'src' in msg:
                src = msg['src']
                for target in msg['pingable']:
                    if target not in ping_targets:
                        ping_targets[target] = {}
                    ping_targets[target][src] = msg['pingable'][target]

    def run_sub():
        Subscriber().bind(obsrv_ex, obsrv_bind).run(sub)

    subscriber_thread = Thread(target=run_sub)
    subscriber_thread.start()

    while 42:
        sleep(1)
        with network_state_lock:
            for target, sources in ping_targets.items():
                if ((target in last_reported) and
                   (config['timeout'] > (now() - last_reported[target]).seconds > (config['ttl'] // 2)) \
                  or target not in last_reported):
                    num_unpingable_sources = sum(int(not pingable) for _, pingable in sources.items())
                    if num_unpingable_sources >= config['unpingable_from_critical']:
                        broadcast_critical(exchange=rprt_ex,
                                           route=target + rprt_route_suffix,
                                           desc=f'{target} is unpingable from {num_unpingable_sources} hosts',
                                           alert_key=f'[{target}][unpingable]',
                                           contents={'target': target,
                                                     'sources': sources},
                                           ttl=config['ttl'])
                        last_reported[target] = now()
                    elif num_unpingable_sources >= config['unpingable_from_error']:
                        broadcast_error(exchange=rprt_ex,
                                        route=target + rprt_route_suffix,
                                        desc=f'{target} is unpingable from {num_unpingable_sources} hosts',
                                        alert_key=f'[{target}][unpingable]',
                                        contents={'target': target,
                                                  'sources': sources},
                                        ttl=config['ttl'])
                        last_reported[target] = now()
                    elif num_unpingable_sources >= config['unpingable_from_warning']:
                        broadcast_warning(exchange=rprt_ex,
                                          route=target + rprt_route_suffix,
                                          desc=f'{target} is unpingable from {num_unpingable_sources} hosts',
                                          alert_key=f'[{target}][unpingable]',
                                          contents={'target': target,
                                                    'sources': sources},
                                          ttl=config['ttl'])
                        last_reported[target] = now()
                    elif num_unpingable_sources > 0:
                        broadcast_info(exchange=rprt_ex,
                                       route=target + rprt_route_suffix,
                                       contents={'target': target,
                                                 'sources': sources})
                        last_reported[target] = now()


if __name__ == '__main__':
    main(load_yaml(stdin.read()))