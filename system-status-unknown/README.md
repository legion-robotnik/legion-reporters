# System Status Unknown

This is a feedback reporter, designed to gather up system information from other reporters analyze it, and fire alerts when a given system has been down for too long (for a configured value of too long). This may imply that a given machine is down, however, this reporter does not guarantee that, it only states that it has not received any system status updates for a given amount of time.

For Example:

```yaml
observe_exchange: skynet,
report_route_suffix: .system.system_reporting,
ttl: 60,
hosts:
  example.com:
    exchange: skynet
    binding: example.com.system.system_base_stats.info
    warning_after_seconds: 60
    error_after_seconds: 300
    critical_after_seconds: 600
  another_host.example.com:
    exchange: skynet
    binding: another_host.example.com.system.system_base_stats.info
    warning_after_seconds: 60
    error_after_seconds: 300
    critical_after_seconds: 600
```

In the configuration above, the reporter will observe two hosts: example.com and another_host.example.com (see the system_base_stats.py reporter for how the hostname is determined). When example.com has not reported its system_base_stats for 60 seconds, this will produce a warning every 30 seconds (half the configured TTL), if it hasn't reported for 300 seconds, the reporter will begin to produce an error, and a critical once it's been 600 seconds since a report was received.

If we had the above saved to a file called `config.yaml` then the script could be executed with:

```bash
python3 system-status-unknown.py < config.yaml
```