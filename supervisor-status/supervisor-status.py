from re import compile as regex
from socket import gethostname
import subprocess
from sys import stdin
from typing import Dict, Union

import legion_utils
from legion_utils import Priority, log
from yaml import safe_load as load_yaml


log.enable("robotnikmq")
log.enable("legion_utils")


HOSTNAME = gethostname()
STATUS_PAT = regex(r'(?P<process>\S+)\s+(?P<state>RUNNING|STARTING|STOPPED|BACKOFF|STOPPING|EXITED|FATAL|UNKNOWN)\s+(?P<status>(pid (?P<pid>\d+)\,\s+uptime (?P<uptime>.+))?.*)')


def sup_status() -> Dict[str, Union[str, Dict[str, Dict[str, str]]]]:
    report: Dict[str, Union[str, Dict[str, Dict[str, str]]]] = {'hostname': HOSTNAME}
    processes: Dict[str, Dict[str, str]] = {}
    report['processes'] = processes
    for line in subprocess.run(['supervisorctl', 'status'], capture_output=True).stdout.decode().split('\n'):
        if m := STATUS_PAT.match(line):
            processes[m.group('process')] = {'state': m.group('state'),
                                             'status': m.group('status')}
            if m.group('pid'):
                processes[m.group('process')]['pid'] = m.group('pid')
            if m.group('uptime'):
                processes[m.group('process')]['uptime'] = m.group('uptime')
    return report


def main(config):
    report = sup_status()
    legion_utils.broadcast_info(exchange=config['exchange'], route=config['route'], contents=report)
    for process_name in config['processes']:
        if process_name not in report['processes']:
            legion_utils.broadcast_error(exchange=config['exchange'],
                                         route=config['route'],
                                         desc=f'{process_name} is not configured with supervisor on {HOSTNAME}',
                                         alert_key=f'[{HOSTNAME}][supervisor][{process_name}_not_configured]',
                                         contents={'unconfigured_process': process_name,
                                                   'hostname': HOSTNAME},
                                         ttl=config['ttl'])
        else:
            state = report['processes'][process_name]['state']
            priority = Priority.__members__[config['processes'][process_name][state].upper()] if state in config['processes'][process_name] else Priority.INFO
            if priority >= 2:
                legion_utils.broadcast_alert(exchange=config['exchange'],
                                             route=config['route'],
                                             priority=priority,
                                             description=f'Supervisor-managed process: {process_name} is in an undesirable state: {state.upper()}',
                                             alert_key=f'[{HOSTNAME}][supervisor][{process_name}][{state}]',
                                             contents={'hostname': HOSTNAME,
                                                       'problematic_process': report['processes'][process_name]},
                                             ttl=config['ttl'])
            elif priority > 0:
                legion_utils.broadcast(exchange=config['exchange'],
                                       priority=priority,
                                       route=config['route'],
                                       contents={'hostname': HOSTNAME,
                                                 'problematic_process': report['processes'][process_name]},
                                       ttl=config['ttl'])

if __name__ == '__main__':
    main(load_yaml(stdin.read()))