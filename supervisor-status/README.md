# Supervisor Status

This reporter is designed to report on processes managed by supervisord: http://supervisord.org/

This reporter will broadcast information about all the processes running on the system via
supervisor, however, you can also configure specific process statuses to prompt alerts by
specifying a given status and the priority at which a message about it should be broadcast.
All unspecified statuses/processes will be broadcast at the lowest priority.

```yaml
exchange: skynet
route: my_server.system.supervisor
ttl: 60
processes:
caddy2:
    STOPPED: warning
    BACKOFF: error
    STOPPING: warning
    EXITED: error
    FATAL: error
    UNKNOWN: error
legiond:
    STOPPED: error
    BACKOFF: critical
    STOPPING: error
    EXITED: critical
    FATAL: critical
    UNKNOWN: critical
```

In the example above, we care about 2 processes: caddy2 and legiond. Caddy is important, so whenever
its stopped/stopping, that's a warning, however, if its not running for any other reason, that's an
error. On the other hand, legiond is far more important (its how we monitor the system) so the
priorities for anything other than running/starting are error and critical.
