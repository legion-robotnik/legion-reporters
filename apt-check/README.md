# Apt Check Reporter

This reporter checks for the number of upgradable and security-upgradable packages, in apt and reports on them. It also alerts if the number of security-upgradeable packages is above a certain threashold.

## Configuration:

The reporter takes in a YAML-formatted file on stdin, for example:

```yaml
security_warning_threshold: 3
security_error_threshold: 10
ttl: 600
queue: skynet
route: my_host.system.apt
```

This example demonstrates a basic configuration which will report the number of upgradable packages and alert on them if the number of security packages is above 3 with a warning or an error if its above 10.


If we had the above saved to a file called `config.yaml` then the script could be executed with:

```bash
python3 apt-check.py < config.yaml
```
