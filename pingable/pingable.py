from socket import gethostname
from subprocess import run, PIPE
from sys import stdin

from legion_utils import log, broadcast_info
from yaml import safe_load as load_yaml


log.enable("robotnikmq")
log.enable("legion_utils")


def pingable(host: str, count: int) -> bool:
    return run(['ping', '-c', str(count), '-w', '10', host],
               stdout=PIPE, stderr=PIPE,
               timeout=15).returncode == 0


def main(config):
    broadcast_info(config['queue'], config['route'],
                   contents={'src': gethostname(),
                   'pingable': {target: pingable(target, config['count']) for target in config['ping']}})

if __name__ == '__main__':
    main(load_yaml(stdin.read()))