#!/usr/bin/env python3
from socket import gethostname
from typing import Dict
from sys import stdin

from legion_utils import Priority, NotificationMsg, broadcast_msg, log
from robotnikmq import Subscriber
from yaml import safe_load as load_yaml

HOSTNAME = gethostname()
log.enable("robotnikmq")
log.enable("legion_utils")


def main(config: Dict) -> None:
    sub = Subscriber().bind(config['observe_exchange'], config['observe_binding'])
    exchange = config['report_exchange']
    route = config['report_route']
    priority = Priority.__members__[config['priority'].upper()]
    for msg in sub.consume():
        if msg is not None and 'priority' in msg:
            if msg['priority'] > 1:
                if 'alert_key' not in msg:
                    broadcast_msg(exchange,
                                  route,
                                  NotificationMsg(contents={'src': HOSTNAME,
                                                            'problem_message': msg},
                                                  alert_key=f'[{msg.route}][alert_without_key]',
                                                  desc=f'Message of priority {msg["priority"]} is missing alert key',
                                                  ttl=config['ttl'],
                                                  priority=priority))
                if 'description' not in msg:
                    broadcast_msg(exchange,
                                  route,
                                  NotificationMsg(contents={'src': HOSTNAME,
                                                              'problem_message': msg},
                                                  alert_key=f'[{msg.route}][alert_without_description]',
                                                  desc=f'Message of priority {msg["priority"]} is missing description',
                                                  ttl=config['ttl'],
                                                  priority=priority))
                if 'ttl' not in msg:
                    broadcast_msg(exchange,
                                  route,
                                  NotificationMsg(contents={'src': HOSTNAME,
                                                            'problem_message': msg},
                                                  alert_key=f'[{msg.route}][alert_without_ttl]',
                                                  desc=f'Message of priority {msg["priority"]} is missing TTL',
                                                  ttl=config['ttl'],
                                                  priority=priority))


if __name__ == '__main__':
    main(load_yaml(stdin.read()))