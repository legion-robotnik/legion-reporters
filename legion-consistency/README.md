# Legion Consistency

This is a feedback reporter, designed to monitor messages published to rabbitMQ exchanges and alert on inconsistent properties. This feedback reporter is good for identifying improperly written alerts, such as alerts with a priority of ERROR, WARNING, or CRITICAL that lack other required properties such as TTL, description, or an alert_key. Note that you can always configure multiple such reporters for different priorities of alerts on different exchanges and bindings.

For Example:

```
[reporter:legion-consistency]
file_url=https://gitlab.com/legion-robotnik/basic-reporters/-/raw/main/reporters/legion_consistency.py
...
config={"observe_exchange": "skynet",
        "observe_binding": "#",
        "report_exchange": "skynet.legion",
        "report_route": "mr-bones.meta.legion_consistency",
        "priority": "warning",
        "ttl": 60}
...
```

In the configuration above, the reporter will monitor the skynet exchange (all routing keys) and then, whenever an inconsistent message is received, a warning will be reported to the skynet.legion exchange with the configured report_route.

If we had the above saved to a file called `config.yaml` then the script could be executed with:

```bash
python3 legion-consistency.py < config.yaml
```